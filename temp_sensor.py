    import os
    import glob
    import time
    import smtplib
    import ssl

    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')

    base_dir = '/sys/bus/w1/devices/'
    device_folder = glob.glob(base_dir + '28*')[0]
    device_file = device_folder + '/w1_slave'

    # Configure your EMail server information here.
    port = 465  # For SSL
    smtp_server = "smtp.gmail.com"
    sender_email = "no-reply@example.org"  # Enter your address
    receiver_email = "notifications@example.org"  # Enter receiver address
    password = "PUTYOURPASSWORDHERE"


    def read_temp_raw():
        f = open(device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_temp():
        lines = read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
            temp_f = temp_c * 9.0 / 5.0 + 32.0
            # Change this line to return temp_c if you prefer celsius
            return temp_f

    def sendmail(temp):
        message = "Subject: Temperature Alert: " + str(temp)
        print(message)  
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, message)


    # Buffer_Base is the minimum amount of time, in seconds, between alerts.
    # This is so you don't get spammed with emails every second.
    buffer_base = 900
    buffer = buffer_base
    # This is the temperature at which Emails will be sent.
    alarm_temp = 75

    while True:
        current_temp = read_temp()
        print(current_temp)     
        time.sleep(1)
        if (buffer < buffer_base):
            buffer = buffer + 1
        if (current_temp >= alarm_temp) and (buffer >= buffer_base):
            sendmail(current_temp)
            buffer = 0