## Raspberry Pi Temperature Sensor (With Email Alerts)
You will need a Raspberry Pi, along with a standard 3-pin Temp Sensor.
[More information on setup available here](https://wcgw.ghost.io/get-high-temp-alerts-via-raspberry-pi/)

1) Install Python, [Enable 1-WIRE](https://pinout.xyz/pinout/1_wire), connect your sensor.
2) Edit temp_sensor.py to your liking. Configure IMAP settings and alarm temp.
3) Place temp_sensor.service in /etc/systemd/system/
4) Edit temp_sensor.service to point to your temp_sensor.py script.
5) Enable the systemd service